# **** This repository is a static version and will no longer be updated ****
# For source code and development version please visit: https://github.com/temehi/dream_yara
----------------------
----------------------
----------------------
----------------------
----------------------


# DREAM-Yara
# An exact read mapper for very large databases with short update time

## Installation

Requirements: python >= 3.5 (TaxSBP), binpacking (TaxSBP)

First, clone this repository:
        
`git clone https://gitlab.com/pirovc/dream_yara.git`

Second, clone TaxSBP (evaluated with v0.05 - https://github.com/pirovc/taxsbp/archive/taxsbp_v0.05.tar.gz):

`git clone https://github.com/pirovc/taxsbp`

You can install binpacking, e.g., by using pip:

`pip install binpacking==1.3`

## Example

We provide an example dataset in `sample_data`. In the following we show how to index, map and update the index using the example dataset.

##### Indexing
We want to index all FASTA files in `sample_data/1/` and output the indices in `index/`. Additionally, we do not want to provide the NCBI taxonomy files, but rather download them automatically. We also pass the links to the TaxSBP and yara folders. We further require that the the bins are no bigger than 4.7M bp (alternatively we could define a number of bins that should be generated) and allow the process to use 12 threads.
```bash
./yara_indexer_dis.py   create \
                        -i sample_data/1/*.fna.gz \
                        -o index/ \
                        --retrieve-ncbi \
                        --taxsbp-path taxsbp/ \
                        --yara-path yara/ \
                        --bin-length 4700000 \
                        -t 12
```
This will generate 128 bins in the `index` folder.

##### Mapping
We now want to map the paired reads `sample_data/simreads.1.fq` and `sample_data/simreads.2.fq` against our just built indices in `index/`. We want to use 12 threads for the mapping against the 128 bins and save the resulting alignment file in `output.sam`.
```bash
yara/yara_mapper_dis    -t 12  \
                        -o output.sam \
                        -fi index/bloom.filter \
                        -e 3 \
                        index/ \
                        sample_data/simreads.1.fq \
                        sample_data/simreads.2.fq
```

##### Updating the index
We can also update the index by providing the path to new references via the `-i` flag or a file containing the accessions of references to be removed via `--remove-file`. `--distribute` will try to distribute the new sequences among more bins which will result in more evenly sized bins. Without this option, fewer bins will be updated.
```bash
./yara_indexer_dis.py   update \
                        -i sample_data/2/*.fna.gz \
                        --remove-file sample_data/2/taxsbp_remove.txt \
                        -o index/ \
                        --retrieve-ncbi \
                        --distribute \
                        --taxsbp-path taxsbp/ \
                        --yara-path yara/ \
                        -t 12
```

## Repository Content
```
dream_yara
├── LICENSE
├── README.md
├── sample_data
│   ├── 1
│   │   ├── GCF_000002495.2_MG8_genomic.fna.gz          # Reference 1
│   │   ├── ...                                         # 31 more references
│   │   ├── GCF_000315875.1_ASM31587v1_genomic.fna.gz   # Reference 33
│   │   └── taxsbp.txt                                  # Example TaxSBP output
│   ├── 2
│   │   ├── GCF_000315915.1_ASM31591v1_genomic.fna.gz   # Reference 1
│   │   ├── ...                                         # 5 more references
│   │   ├── GCF_001672515.1_ASM167251v1_genomic.fna.gz  # Reference 7
│   │   ├── taxsbp_add.txt                              # Example TaxSBP output for adding references
│   │   └── taxsbp_remove.txt                           # Example TaxSBP output for removing references
│   ├── simreads.1.fq                                   # 10K simulated paired reads, first mate
│   └── simreads.2.fq                                   # 10K simulated paired reads, second mate
├── yara                                                # Yara for indexing and mapping
│   ├── yara_build_filter                               # Build a bloom/k-mer filter from bins
│   ├── yara_indexer                                    # Non-distributed indexer
│   ├── yara_indexer_dis                                # Create index for each bin
│   ├── yara_mapper                                     # Non-distributed mapper
│   ├── yara_mapper_dis                                 # Map reads to reference
│   └── yara_update_filter                              # Update bloom/k-mer filter
└── yara_indexer_dis.py                                 # Wrapper for creating bins and index
```

